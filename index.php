<html>
  <head>
    <title>Student Info</title>
  </head>
  <body>
  <div>
    <form action="add_stud.php" method="POST">
      <button type="submit" name="add" id="add_stud">Add Record</button>
    </form>
  </div>
  <table class="table">
  <thead>
    <tr>
      <th>Sr.No</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Location</th>
      <th> Actions </th>
    </tr>
  </thead>
  <tbody>
    <?php
      require 'conn.php';
      $sql = "select * from stud_info";
      $result = mysqli_query($conn,$sql);
      $cnt = 0 ; 
      
      if (mysqli_num_rows($result) > 0)
      {
        while($row = mysqli_fetch_assoc($result))
        {
          $cnt ++;
    ?>
    <tr>
      <th scope="row"><?php echo $cnt ?></th>
      <td><?php echo $row['first_name'] ?></td>
      <td><?php echo $row['last_name'] ?></td>
      <td><?php echo $row['location'] ?></td>
      <td>
        <form action="delete_stud.php" method="POST">
          <input type="hidden" value="<?php echo $row['id']?>" name="delete_id"/>
          <button type="submit" name="delete">Delete</button>
        </form>
      </td>
      <td>
        <form action="update_stud.php" method="POST">
          <input type="hidden" value="<?php echo $row['id']?>" name="update_id"/>
          <button type="submit" name="update">Update</button>
        </form>
      </td>

    </tr>
    <?php
        }
      }
    ?>
  </tbody>
</table>
  </body>  
</html>